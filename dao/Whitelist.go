package dao

type Whitelist struct {
	ServerUuid string `json:"serveruuid" xorm:"pk notnull unique 'serveruuid'"`
	ServerIP   string `json:"serverip" xorm:"'serverip'"`
	ServerPort int    `json:"serverport" xorm:"'serverport'"`
	Created    int64  `json:"created" xorm:"'created'"`
	Deleted    int    `json:"deleted" xorm:"default(0) 'deleted'"`
	Token      string `json:"token" xorm:"'token'"`
}
