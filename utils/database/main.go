package db

import (
	"os"
	"strings"
	"time"
	"youbei/dao"
	Zipz "youbei/utils/zip"
)

// SQLDumpZip 将指定的文件或目录打包为ZIP文件，并删除原有的文件或目录
// sourcePath: 需要打包的源文件或目录的路径
// zipFilePath: ZIP文件的生成路径
// zipFilePassword: ZIP文件的密码
// 返回值: ZIP文件的路径，错误信息
func SQLDumpZip(sourcePath, zipFilePath, zipFilePassword string) (string, error) {
	// 打包源文件或目录为ZIP文件
	err := Zipz.Zip(sourcePath, zipFilePath, zipFilePassword)
	if err != nil {
		return zipFilePath, err
	}

	// 删除原有的源文件或目录
	err = os.RemoveAll(sourcePath)
	return zipFilePath, err
}

// GenerateFileNames 根据给定的任务信息，生成SQL文件和ZIP文件的文件名
// taskInfo: 任务信息
// 返回值: SQL文件的文件名，ZIP文件的文件名
func GenerateFileNames(taskInfo *dao.Task) (string, string) {
	// 获取当前时间，格式为"2006-01-02_15-04-05"
	currentTime := time.Now().Format("2006-01-02_15-04-05")

	// 生成SQL文件的文件名
	sqlFileName := taskInfo.SavePath + "/" + taskInfo.Host + "_" + taskInfo.DBType + "_" + taskInfo.DBname + "_" + "_" + currentTime + ".sql"

	// 生成ZIP文件的文件名
	zipFileName := taskInfo.SavePath + "/" + taskInfo.Host + "_" + taskInfo.DBType + "_" + taskInfo.DBname + "_" + "_" + currentTime + ".zip"

	return sqlFileName, zipFileName
}

// FormatPath 对给定的路径进行格式化，对路径中的空格进行处理
// originalPath: 需要格式化的源路径
// 返回值: 格式化后的路径
func FormatPath(originalPath string) string {
	// 将源路径按"/"分割为多个部分
	pathSegments := strings.Split(originalPath, "/")

	// 初始化一个新的切片来存储处理后的路径部分
	formattedSegments := []string{}

	// 遍历所有的路径部分
	for _, segment := range pathSegments {
		// 如果路径部分中包含空格
		if containsSpace := strings.ContainsAny(segment, " "); containsSpace {
			// 将路径部分用双引号包围，并添加到新的切片中
			formattedSegments = append(formattedSegments, `"`+segment+`"`)
		} else {
			// 将路径部分添加到新的切片中
			formattedSegments = append(formattedSegments, segment)
		}
	}

	// 将处理后的路径部分用"/"连接为一个新的路径，并返回
	return strings.Join(formattedSegments, "/")
}
