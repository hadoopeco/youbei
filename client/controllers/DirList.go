package YBClient

import (
	"youbei/utils"

	"github.com/gin-gonic/gin"
)

// DirList ...
func DirList(c *gin.Context) {
	dir := c.Query("dir")
	bol := c.GetBool("isdir")
	if dislist, err := utils.GetDir(dir, bol); err != nil {
		APIReturn(c, 500, "获取目录失败", err)
	} else {
		APIReturn(c, 200, "获取目录成功", dislist)
	}
	return
}
