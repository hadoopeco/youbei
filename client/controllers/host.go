package YBClient

import (
	"net"

	"github.com/gin-gonic/gin"
)

func GetLocal(c *gin.Context) {
	str := []string{}
	addrs, err := net.InterfaceAddrs()
	if err != nil {
		APIReturn(c, 500, "failed", err.Error())
		return
	}
	for _, address := range addrs {
		// 检查ip地址判断是否回环地址
		if ipnet, ok := address.(*net.IPNet); ok {
			if ipnet.IP.To4() != nil {
				str = append(str, ipnet.IP.String())
			}
		}
	}
	str = append(str, "localhost")
	APIReturn(c, 200, "success", str)
}
